process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
const axios = require('axios');
const fs = require('fs');

const BASE_URL = 'https://jservice.xyz/api'

axios.get(`${BASE_URL}/categories`)
    .then(res => {
        fs.writeFile('categories.json', JSON.stringify(res.data.categories), (err) => {
            if (err) throw err;
            console.log("categories written to file!")
        })
    })